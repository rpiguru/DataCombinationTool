import ntpath
import os
import sys
import qdarkstyle

from PySide2.QtWidgets import QMainWindow, QApplication, QFileDialog, QTableWidgetItem

from settings import HOME_DIR, COUNT
from ui_data_combine_tool import Ui_DataCombineTool
from utils.common import logger, show_error_message
from utils.excel import read_excel_file, combine_data


class DataCombinationTool(QMainWindow):

    def __init__(self):
        super().__init__()
        self.ui = Ui_DataCombineTool()
        self.ui.setupUi(self)
        self._file = None
        self._data = None
        self._i_sheet = 0
        self._i_group = 0
        self.ui.action_Open.triggered.connect(self._on_action_open_file)
        self.ui.actionE_xit.triggered.connect(self.close)
        self.ui.btnNext.released.connect(self._on_btn_next)
        self.ui.btnPrevious.released.connect(self._on_btn_previous)
        self.ui.btnProcess.released.connect(self._on_btn_process)
        self.ui.btnProcessSheet.released.connect(self._on_btn_process_sheet)
        self.ui.tableWidget.setRowCount(COUNT)

    def __initialize(self):
        self._i_sheet = 0
        self._i_group = 0
        self.ui.btnProcess.setEnabled(False)

    def _on_action_open_file(self):
        open_file_name = QFileDialog.getOpenFileName(self, "Open Excel file", HOME_DIR, "Excel files (*.xlsx)")
        if open_file_name[0]:
            self._file = open_file_name[0]
            os.makedirs(os.path.join(os.path.dirname(self._file), 'combined'), exist_ok=True)
            self.setWindowTitle(f"{self._file}  -  DataCombineTool")
            self._data = read_excel_file(self._file)
            if self._data:
                self.ui.btnProcess.setEnabled(True)
                self._show_group_data()
            else:
                show_error_message("No Data Found!")
                self.__initialize()

    def _show_group_data(self):
        sheet_data = self._data[self._i_sheet]
        self.ui.lbTitle.setText(f"{sheet_data['name']} - {self._i_group + 1} / {len(sheet_data['groups'])}")
        for row, data in enumerate(sheet_data['groups'][self._i_group]):
            for i in range(6):
                self.ui.tableWidget.setItem(row, i, QTableWidgetItem(str(data[i])))
        if self._i_sheet == 0 and self._i_group == 0:
            self.ui.btnPrevious.setEnabled(False)
        else:
            self.ui.btnPrevious.setEnabled(True)
        if self._i_sheet == len(self._data) - 1 and self._i_group == len(sheet_data['groups']) - 1:
            self.ui.btnNext.setEnabled(False)
        else:
            self.ui.btnNext.setEnabled(True)

    def _on_btn_next(self):
        if self._i_group < len(self._data[self._i_sheet]['groups']) - 1:
            self._i_group += 1
        else:
            self._i_group = 0
            self._i_sheet += 1
        self._show_group_data()

    def _on_btn_previous(self):
        if self._i_group > 0:
            self._i_group -= 1
        else:
            self._i_sheet -= 1
            self._i_group = len(self._data[self._i_sheet]['groups']) - 1
        self._show_group_data()

    def __process_a_group(self, sheet_num, group_num):
        sheet_name = self._data[sheet_num]['name']
        data = self._data[sheet_num]['groups'][group_num]
        file_name = ntpath.basename(self._file).replace('.xlsx', f"_{sheet_name}_{group_num + 1}_2x2.xlsx")
        file_path = os.path.join(os.path.dirname(self._file), 'combined', file_name)
        combine_data(data, file_path)
        file_path = file_path.replace('2x2', '3x3')
        combine_data(data, file_path, num=3)

    def _on_btn_process(self):
        self.__process_a_group(self._i_sheet, self._i_group)

    def _on_btn_process_sheet(self):
        for g_num in range(len(self._data[self._i_sheet]['groups'])):
            self.__process_a_group(self._i_sheet, g_num)


if __name__ == '__main__':

    logger.info('========== Starting Data Combine Tool ==========')

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
    window = DataCombinationTool()
    window.show()

    sys.exit(app.exec_())
