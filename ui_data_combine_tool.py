# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'data_combine_tool.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *

import resource_rc

class Ui_DataCombineTool(object):
    def setupUi(self, DataCombineTool):
        if not DataCombineTool.objectName():
            DataCombineTool.setObjectName(u"DataCombineTool")
        DataCombineTool.resize(731, 895)
        self.actionE_xit = QAction(DataCombineTool)
        self.actionE_xit.setObjectName(u"actionE_xit")
        icon = QIcon()
        icon.addFile(u":/img/img/Turn off.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionE_xit.setIcon(icon)
        self.action_Open = QAction(DataCombineTool)
        self.action_Open.setObjectName(u"action_Open")
        icon1 = QIcon()
        icon1.addFile(u":/img/img/Open.png", QSize(), QIcon.Normal, QIcon.Off)
        self.action_Open.setIcon(icon1)
        self.actionAbout = QAction(DataCombineTool)
        self.actionAbout.setObjectName(u"actionAbout")
        icon2 = QIcon()
        icon2.addFile(u":/img/img/Help book.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionAbout.setIcon(icon2)
        self.centralwidget = QWidget(DataCombineTool)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btnPrevious = QToolButton(self.frame)
        self.btnPrevious.setObjectName(u"btnPrevious")
        self.btnPrevious.setEnabled(False)
        icon3 = QIcon()
        icon3.addFile(u":/img/img/Previous.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnPrevious.setIcon(icon3)
        self.btnPrevious.setIconSize(QSize(40, 40))

        self.horizontalLayout.addWidget(self.btnPrevious)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.lbTitle = QLabel(self.frame)
        self.lbTitle.setObjectName(u"lbTitle")
        font = QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.lbTitle.setFont(font)
        self.lbTitle.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.lbTitle)

        self.tableWidget = QTableWidget(self.frame)
        if (self.tableWidget.columnCount() < 6):
            self.tableWidget.setColumnCount(6)
        if (self.tableWidget.rowCount() < 100):
            self.tableWidget.setRowCount(100)
        self.tableWidget.setObjectName(u"tableWidget")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableWidget.sizePolicy().hasHeightForWidth())
        self.tableWidget.setSizePolicy(sizePolicy)
        self.tableWidget.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.tableWidget.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tableWidget.setRowCount(100)
        self.tableWidget.setColumnCount(6)
        self.tableWidget.horizontalHeader().setVisible(False)
        self.tableWidget.horizontalHeader().setMinimumSectionSize(85)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(85)
        self.tableWidget.horizontalHeader().setStretchLastSection(False)
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setDefaultSectionSize(30)

        self.verticalLayout_2.addWidget(self.tableWidget)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.btnNext = QToolButton(self.frame)
        self.btnNext.setObjectName(u"btnNext")
        self.btnNext.setEnabled(False)
        icon4 = QIcon()
        icon4.addFile(u":/img/img/Next.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnNext.setIcon(icon4)
        self.btnNext.setIconSize(QSize(40, 40))

        self.horizontalLayout.addWidget(self.btnNext)


        self.verticalLayout.addWidget(self.frame)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.btnProcess = QPushButton(self.centralwidget)
        self.btnProcess.setObjectName(u"btnProcess")
        self.btnProcess.setEnabled(False)
        self.btnProcess.setMinimumSize(QSize(0, 40))
        font1 = QFont()
        font1.setPointSize(12)
        self.btnProcess.setFont(font1)
        icon5 = QIcon()
        icon5.addFile(u":/img/img/parts.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btnProcess.setIcon(icon5)
        self.btnProcess.setIconSize(QSize(30, 30))

        self.horizontalLayout_2.addWidget(self.btnProcess)

        self.btnProcessSheet = QPushButton(self.centralwidget)
        self.btnProcessSheet.setObjectName(u"btnProcessSheet")
        self.btnProcessSheet.setEnabled(False)
        self.btnProcessSheet.setMinimumSize(QSize(0, 40))
        self.btnProcessSheet.setFont(font1)
        self.btnProcessSheet.setIcon(icon5)
        self.btnProcessSheet.setIconSize(QSize(30, 30))

        self.horizontalLayout_2.addWidget(self.btnProcessSheet)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        DataCombineTool.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(DataCombineTool)
        self.statusbar.setObjectName(u"statusbar")
        DataCombineTool.setStatusBar(self.statusbar)
        self.toolBar = QToolBar(DataCombineTool)
        self.toolBar.setObjectName(u"toolBar")
        DataCombineTool.addToolBar(Qt.TopToolBarArea, self.toolBar)
        self.menuBar = QMenuBar(DataCombineTool)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 731, 23))
        self.menu_File = QMenu(self.menuBar)
        self.menu_File.setObjectName(u"menu_File")
        self.menuHelp = QMenu(self.menuBar)
        self.menuHelp.setObjectName(u"menuHelp")
        DataCombineTool.setMenuBar(self.menuBar)

        self.toolBar.addAction(self.action_Open)
        self.toolBar.addSeparator()
        self.menuBar.addAction(self.menu_File.menuAction())
        self.menuBar.addAction(self.menuHelp.menuAction())
        self.menu_File.addAction(self.action_Open)
        self.menu_File.addSeparator()
        self.menu_File.addAction(self.actionE_xit)
        self.menuHelp.addAction(self.actionAbout)

        self.retranslateUi(DataCombineTool)

        QMetaObject.connectSlotsByName(DataCombineTool)
    # setupUi

    def retranslateUi(self, DataCombineTool):
        DataCombineTool.setWindowTitle(QCoreApplication.translate("DataCombineTool", u"Data Combine Tool", None))
        self.actionE_xit.setText(QCoreApplication.translate("DataCombineTool", u"E&xit", None))
#if QT_CONFIG(shortcut)
        self.actionE_xit.setShortcut(QCoreApplication.translate("DataCombineTool", u"Ctrl+X", None))
#endif // QT_CONFIG(shortcut)
        self.action_Open.setText(QCoreApplication.translate("DataCombineTool", u"&Open", None))
#if QT_CONFIG(shortcut)
        self.action_Open.setShortcut(QCoreApplication.translate("DataCombineTool", u"Ctrl+O", None))
#endif // QT_CONFIG(shortcut)
        self.actionAbout.setText(QCoreApplication.translate("DataCombineTool", u"&About", None))
#if QT_CONFIG(shortcut)
        self.actionAbout.setShortcut(QCoreApplication.translate("DataCombineTool", u"Ctrl+H", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.btnPrevious.setToolTip(QCoreApplication.translate("DataCombineTool", u"Previous Sheet", None))
#endif // QT_CONFIG(tooltip)
        self.btnPrevious.setText("")
        self.lbTitle.setText(QCoreApplication.translate("DataCombineTool", u"Open the Excel File", None))
#if QT_CONFIG(tooltip)
        self.btnNext.setToolTip(QCoreApplication.translate("DataCombineTool", u"Next Sheet", None))
#endif // QT_CONFIG(tooltip)
        self.btnNext.setText("")
        self.btnProcess.setText(QCoreApplication.translate("DataCombineTool", u"Combine This", None))
        self.btnProcessSheet.setText(QCoreApplication.translate("DataCombineTool", u"Combine Sheet", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("DataCombineTool", u"toolBar", None))
        self.menu_File.setTitle(QCoreApplication.translate("DataCombineTool", u"&File", None))
        self.menuHelp.setTitle(QCoreApplication.translate("DataCombineTool", u"Help", None))
    # retranslateUi

