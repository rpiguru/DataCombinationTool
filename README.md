# DataCombinationTool

DataCombinationTool

## Installation

- Install dependencies

    ```shell script
    python3 -m pip install -r requirements.txt
    ```

## Launch application

    ```shell script
    python3 main.py
    ```
