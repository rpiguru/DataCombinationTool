import os
import sys
import logging.config

from PySide2.QtWidgets import QMessageBox

_cur_dir = os.path.dirname(os.path.realpath(__file__))
_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('DC')


def show_error_message(msg):
    m = QMessageBox()
    m.setWindowTitle("DataCombineTool")
    m.setIcon(m.Icon.Warning)
    m.setText(msg)
    m.exec_()
