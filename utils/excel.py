import collections
from openpyxl import load_workbook, Workbook
from openpyxl.styles import Font, PatternFill
from itertools import combinations

from openpyxl.styles.colors import YELLOW, RED, GREEN, BLUE

from settings import COUNT


def _parse_sheet(ws):
    groups = []
    n = 1
    while True:
        buf = []
        for row in range(COUNT):
            try:
                values = [int(ws.cell(row + 1, n + i).value) for i in range(6)]
            except (ValueError, TypeError):
                break
            if all(values):
                buf.append(values)
            else:
                break
        if len(buf) == COUNT:
            groups.append(buf)
            n += 7
        else:
            break

    return groups


def read_excel_file(file_path):
    wb = load_workbook(filename=file_path)
    result = []
    for sheet_name in wb.sheetnames:
        sheet_data = _parse_sheet(wb[sheet_name])
        if sheet_data:
            result.append({"name": sheet_name, "groups": sheet_data})
    return result


def _sort_cell(values, num=2):

    if num == 2:
        return sorted(values, key=lambda v: int(v[:2]) * 100 + int(v[2:]))
    else:
        return sorted(values, key=lambda v: int(v.split()[0]) * 10000 + int(v.split()[1]) * 100 + int(v.split()[2]))


def list_duplicates(seq, num=2):
    unique = [item for item, count in collections.Counter(seq).items() if count == 1]
    return _sort_cell([v for v in seq if v not in unique], num=num)


def _create_sheets(data, wb, cnt=6, num=2):
    # ========== Combination Sheet ==========
    loc = {}
    buf = []
    ws_comb = wb.create_sheet(f"({cnt}){num}x{num}")
    for i, row in enumerate(data):
        s_row = 1 + (12 if cnt == 5 else 17) * (i // 20)
        col = i % 20 + 1
        ws_comb.cell(row=s_row, column=col, value=str(i + 1))
        ws_comb.cell(row=s_row, column=col).font = Font(color="009400D3")
        comb = sorted([" ".join([str(m) for m in sorted(v)]) for v in combinations(row[:cnt], num)])
        for j, d in enumerate(comb):
            ws_comb.cell(row=s_row + j + 1, column=col, value=d)
            if d not in loc:
                loc[d] = [i + 1]
            else:
                loc[d].append(i + 1)
        buf.extend(comb)
    buf = _sort_cell(buf, num=num)

    # ========== Duplicate Sheet ==========
    ws_dup = wb.create_sheet(f"({cnt})Dup")
    ws_dup.cell(row=1, column=1, value="Duplicate")
    ws_dup.cell(1, 1).fill = PatternFill(start_color=YELLOW, end_color=YELLOW, fill_type='solid')

    dup_buf = list_duplicates(buf, num=num)
    for j, val in enumerate(dup_buf):
        ws_dup.cell(row=2 + j, column=1, value=val)
        ws_dup.cell(row=2 + j, column=1).fill = \
            PatternFill(start_color="00F08080", end_color="00F08080", fill_type='solid')

    ws_dup.cell(row=1, column=4, value="Unique")
    ws_dup.cell(1, 4).fill = PatternFill(start_color=YELLOW, end_color=YELLOW, fill_type='solid')

    u_buf = _sort_cell([v for v in buf if v not in dup_buf], num=num)
    for j, val in enumerate(u_buf):
        ws_dup.cell(row=2 + j, column=4, value=val)

    # ========== Red Sheet ==========
    ws_red = wb.create_sheet(f"({cnt}){num}x{num}red")
    max_val = max(list(set([int(v[:2]) for v in dup_buf])))
    dd_buf = [[] for _ in range(max_val + 1)]
    for v in dup_buf:
        dd_buf[int(v[:2])].append(v)

    for col_v in range(max_val):
        ws_red.cell(row=1, column=col_v + 1, value=str(col_v + 1))
        for j, v in enumerate(dd_buf[col_v + 1]):
            cell = ws_red.cell(row=2 + j, column=col_v + 1)
            cell.value = v
            count = dd_buf[col_v + 1].count(v)
            if count == 3:
                cell.font = Font(color=RED)
            elif count == 5:
                cell.font = Font(color=GREEN)
            elif count > 5:
                cell.font = Font(color=BLUE)

    # ========== Unique Sheet ==========
    ws_unq = wb.create_sheet(f"({cnt})Uni")
    max_val = max(list(set([int(v[:2]) for v in u_buf])))
    uu_buf = [[] for _ in range(max_val + 1)]
    for v in u_buf:
        uu_buf[int(v[:2])].append(v)
    for col_v in range(max_val):
        ws_unq.cell(row=1, column=col_v + 1, value=str(col_v + 1))
        for j, v in enumerate(uu_buf[col_v + 1]):
            ws_unq.cell(row=2 + j, column=col_v + 1, value=v)

    # ========== Count & Loc Sheet ==========
    ws_cnt = wb.create_sheet(f"({cnt})Counter of dup")
    cl_buf = _sort_cell(list(set(dup_buf)), num=num)
    cl_buf.sort(key=lambda v: len(loc[v]), reverse=True)
    ws_cnt.cell(row=1, column=1, value="Comb")
    ws_dup.cell(1, 1).fill = PatternFill(start_color=YELLOW, end_color=YELLOW, fill_type='solid')
    ws_cnt.cell(row=1, column=2, value="Count")
    ws_dup.cell(1, 2).fill = PatternFill(start_color=YELLOW, end_color=YELLOW, fill_type='solid')

    for j, row in enumerate(cl_buf):
        ws_cnt.cell(row=j + 2, column=1, value=row)
        ws_cnt.cell(row=j + 2, column=1).fill = \
            PatternFill(start_color="00F08080", end_color="00F08080", fill_type='solid')
        ws_cnt.cell(row=j + 2, column=2, value=len(loc[row]))
        for q, v in enumerate(loc[row]):
            ws_cnt.cell(row=j + 2, column=q + 3, value=v)


def combine_data(data, file_path, num=2):
    wb = Workbook()
    # ========== Tick Sheet ==========
    ws_tick = wb.active
    ws_tick.title = 'Tick'
    for i, row in enumerate(data):
        for j, v in enumerate(row):
            ws_tick.cell(row=i + 1, column=j + 1, value=v)

    _create_sheets(data=data, wb=wb, cnt=6, num=num)
    _create_sheets(data=data, wb=wb, cnt=5, num=num)

    wb.save(file_path)


if __name__ == '__main__':

    import os

    # read_excel_file(os.path.expanduser('~/Documents/DataCombineTool/1.xlsx'))
    _wb = load_workbook(filename=os.path.expanduser('~/Documents/DataCombineTool/test_input.xlsx'))
    _ws = _wb.active
    _data = []
    for k in range(COUNT):
        _data.append([_ws.cell(k + 1, kk + 1).value for kk in range(6)])

    combine_data(_data, file_path=os.path.expanduser('~/Documents/DataCombineTool/combined/test.xlsx'), num=3)
